//Libs
//=include ../../node_modules/jquery/dist/jquery.min.js
//=include ../../node_modules/slick-carousel/slick/slick.js
//=include ../../node_modules/object-fit-images/dist/ofi.min.js

//Partials
//=include partials/search-form_actions.js
//=include partials/main-slider_init.js
//=include partials/object-fit_init.js
//=include partials/look-slider_init.js
//=include partials/featured-slider_init.js
//=include partials/header_scroll-functionality.js
//=include partials/recent-posts-slider_init.js
//=include partials/navigation-button_functionality.js
//=include partials/navigation_functionality.js
//=include partials/btn-contact-mobile_functionality.js
//=include partials/footer_functionality.js
//=include partials/scroll-to-anchor.js