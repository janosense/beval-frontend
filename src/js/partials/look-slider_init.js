$(document).ready(function () {

  var lookSlider = $('.look__slider');

  lookSlider.slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    prevArrow: "<div class=\"look__control look__control--prev\"></div>",
    nextArrow: "<div class=\"look__control look__control--next\"></div>",
    responsive: [
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 530,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });

  if ($(window).width() < 640) {
    lookSlider.slick('slickFilter','.look__item--bg');
  }

  $(window).on('resize', function() {
    if ($(window).width() < 640) {
      lookSlider.slick('slickFilter','.look__item--bg');
    } else {
      lookSlider.slick('slickUnfilter');
    }
  });

});
