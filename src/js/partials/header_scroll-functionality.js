(function () {
  var cntHeader = document.getElementById('cnt__header');
  if (cntHeader) {

    var logo = document.getElementById('logo');
    var logoCurrentSrc = logo.getAttribute('src');
    var logoSize = [];
    logoSize.push(logo.getAttribute('width'));
    logoSize.push(logo.getAttribute('height'));

    var logoTablet = document.getElementById('logo-tablet');
    var logoTabletCurrentSrc = logoTablet.getAttribute('src');
    var logoTabletSize = [];
    logoTabletSize.push(logoTablet.getAttribute('width'));
    logoTabletSize.push(logoTablet.getAttribute('height'));

    var logoMobile = document.getElementById('logo-mobile');
    var logoMobileCurrentSrc = logoMobile.getAttribute('src');
    var logoMobileSize = [];
    logoMobileSize.push(logoMobile.getAttribute('width'));
    logoMobileSize.push(logoMobile.getAttribute('height'));

    var scrollTop = window.pageYOffset;
    var isPageTop = false;
    if (scrollTop > 50 && !isPageTop) {
      cntHeader.classList.add('header--onscroll');
      if (document.documentElement.clientWidth > 1020) {
        logo.setAttribute('src', logo.getAttribute('data-logo-condensed'));
        logo.setAttribute('width', '57');
        logo.setAttribute('height', '57');
        isPageTop = true;
      } else if (document.documentElement.clientWidth > 450) {
        logoTablet.setAttribute('src', logoTablet.getAttribute('data-logo-condensed'));
        logoTablet.setAttribute('width', '57');
        logoTablet.setAttribute('height', '57');
        isPageTop = true;
      } else {
        logoMobile.setAttribute('src', logoMobile.getAttribute('data-logo-condensed'));
        logoMobile.setAttribute('width', '32');
        logoMobile.setAttribute('height', '32');
        isPageTop = true;
      }
    }

    document.addEventListener('scroll', function () {
      scrollTop = window.pageYOffset;

      if (scrollTop > 50 && !isPageTop) {
        cntHeader.classList.add('header--onscroll');
        if (document.documentElement.clientWidth > 1020) {
          logo.setAttribute('src', logo.getAttribute('data-logo-condensed'));
          logo.setAttribute('width', '57');
          logo.setAttribute('height', '57');
          isPageTop = true;
        } else if (document.documentElement.clientWidth > 450) {
          logoTablet.setAttribute('src', logoTablet.getAttribute('data-logo-condensed'));
          logoTablet.setAttribute('width', '57');
          logoTablet.setAttribute('height', '57');
          isPageTop = true;
        } else {
          logoMobile.setAttribute('src', logoMobile.getAttribute('data-logo-condensed'));
          logoMobile.setAttribute('width', '32');
          logoMobile.setAttribute('height', '32');
          isPageTop = true;
        }
      } else if (scrollTop < 50 && isPageTop) {
        cntHeader.classList.remove('header--onscroll');
        if (document.documentElement.clientWidth > 1020) {
          setTimeout(function () {
            logo.setAttribute('src', logoCurrentSrc);
            logo.setAttribute('width', logoSize[0]);
            logo.setAttribute('height', logoSize[1]);
          }, 300);
          isPageTop = false;
        } else if (document.documentElement.clientWidth > 450) {
          setTimeout(function () {
            logoTablet.setAttribute('src', logoTabletCurrentSrc);
            logoTablet.setAttribute('width', logoTabletSize[0]);
            logoTablet.setAttribute('height', logoTabletSize[1]);
          }, 300);
          isPageTop = false;
        } else {
          setTimeout(function () {
            logoMobile.setAttribute('src', logoMobileCurrentSrc);
            logoMobile.setAttribute('width', logoMobileSize[0]);
            logoMobile.setAttribute('height', logoMobileSize[1]);
          }, 300);
          isPageTop = false;
        }
      }
    });
  }
})();