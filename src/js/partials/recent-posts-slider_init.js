$(document).ready(function () {

  $('.recent-posts__slider').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    prevArrow: "<div class=\"recent-posts__control recent-posts__control--prev\"></div>",
    nextArrow: "<div class=\"recent-posts__control recent-posts__control--next\"></div>",
    responsive: [
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 490,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  });
});
