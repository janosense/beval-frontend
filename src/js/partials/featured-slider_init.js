$(document).ready(function () {

  var featuredSlider = $('.featured__slider');

  featuredSlider.slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    prevArrow: "<div class=\"featured__control featured__control--prev\"></div>",
    nextArrow: "<div class=\"featured__control featured__control--next\"></div>",
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 550,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 380,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });

  featuredSlider.slick('slickFilter','.featured-products');

  $('.new-arrivals').on('click', function(){
    var featuredSlider = $('.featured__slider');
    featuredSlider.slick('slickUnfilter');
    featuredSlider.slick('slickFilter','.new-arrivals');
    $('.featured__filter-button').removeClass('featured__filter-button--active');
    $(this).addClass('featured__filter-button--active');
  });

  $('.featured-products').on('click', function(){
    var featuredSlider = $('.featured__slider');
    featuredSlider.slick('slickUnfilter');
    featuredSlider.slick('slickFilter','.featured-products');
    $('.featured__filter-button').removeClass('featured__filter-button--active');
    $(this).addClass('featured__filter-button--active');
  });

  $('.best-sellers').on('click', function(){
    var featuredSlider = $('.featured__slider');
    featuredSlider.slick('slickUnfilter');
    featuredSlider.slick('slickFilter','.best-sellers');
    $('.featured__filter-button').removeClass('featured__filter-button--active');
    $(this).addClass('featured__filter-button--active');
  });

});
