(function () {
  var footerCaptionsList = document.getElementsByClassName('footer__caption');
  if (footerCaptionsList) {
    for (var i = 0; i < footerCaptionsList.length; i++) {
      footerCaptionsList[i].setAttribute('data-state', 'disabled');
      footerCaptionsList[i].addEventListener('click', function (event) {
        var target = event.target;
        var dropdownState = this.getAttribute('data-state');
        if (dropdownState === 'disabled') {
          // for (var i = 0; i < footerCaptionsList.length; i++) {
          //   footerCaptionsList[i].setAttribute('data-state', 'disabled');
          // }
          this.setAttribute('data-state', 'enabled');
        } else {
          this.setAttribute('data-state', 'disabled');
        }
      });
    }
  }
})();
