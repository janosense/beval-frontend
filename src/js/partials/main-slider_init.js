$(document).ready(function () {

  $('.main-slider__list').slick({
    dots: true,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: false,
  });
});