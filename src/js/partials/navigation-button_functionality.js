(function () {
  var navigationButton = document.getElementById('btn__navigation');
  if (navigationButton) {

    var body = document.body;
    var isActive = false;
    var navigation = document.getElementById('cnt__navigation');
    var cntBottomBar = document.getElementById('cnt__bottom-bar');
    navigationButton.addEventListener('click', function() {

      if (!isActive) {
        this.classList.toggle('navigation-button--onclick');
        cntBottomBar.classList.add('header__bottom-bar--active');
        setTimeout(function() {
          navigationButton.classList.toggle('navigation-button--active');
        }, 400);
        isActive = !isActive;
        body.classList.add('no-scroll');
      } else {
        this.classList.toggle('navigation-button--active');
        cntBottomBar.classList.remove('header__bottom-bar--active');
        setTimeout(function() {
          navigationButton.classList.toggle('navigation-button--onclick');
        }, 400);
        body.classList.remove('no-scroll');
        isActive = !isActive;
      }
    });

    document.addEventListener('click', function(event) {
      var target = event.target;
      while (target !== this) {
        if (target === navigation || target === navigationButton) {
          return false;
        }
        target = target.parentNode;
      }
      navigationButton.classList.remove('navigation-button--active');
      cntBottomBar.classList.remove('header__bottom-bar--active');
      setTimeout(function() {
        navigationButton.classList.remove('navigation-button--onclick');
      }, 400);
      body.classList.remove('no-scroll');
      isActive = false;
    });
  }
})();