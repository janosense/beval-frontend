(function () {
  var buttonCategories = document.getElementById('btn__categories');
  var buttonFilters = document.getElementById('btn__filters');
  var categoriesLayout = document.getElementById('cnt__categories');
  var filtersLayout = document.getElementById('cnt__filters');

  if (buttonCategories && categoriesLayout) {
    var categories = document.getElementsByClassName('filter__categories-item');
    var subCategories = document.getElementsByClassName('filter__sub-categories-item');

    buttonCategories.addEventListener('click', function(event) {
      categoriesLayout.classList.toggle('filter__layout--active');
      buttonFilters.setAttribute('data-state', 'disabled');
      if (document.documentElement.clientWidth < 700) {
        buttonFilters.classList.toggle('filter__button--hidden');
      }

      if (filtersLayout.classList.contains('filter__layout--active')) {
        filtersLayout.classList.remove('filter__layout--active')
      }
      if (this.getAttribute('data-state') === 'disabled') {
        this.setAttribute('data-state', 'enabled');
      } else {
        this.setAttribute('data-state', 'disabled');
      }
    });

    categoriesLayout.addEventListener('click', function(event) {
      var target = event.target;
      while(target !== this) {
        if (target.tagName === 'LI') {
          target.classList.toggle('active');
          return;
        }
        target = target.parentNode;
      }
    });

    for (var i = 0; i < subCategories.length; i++) {
      subCategories[i].addEventListener('click', function() {
        removeCssClass(subCategories, 'filter__sub-categories-item--active');
        this.classList.add('filter__sub-categories-item--active')
      })
    }
  }

  if (buttonFilters && filtersLayout) {
    var filters = document.getElementById('cnt__filter-items');
    var filterItems = document.getElementsByClassName('filter__item');
    var subFilters = document.getElementsByClassName('filter__sub-filters'); //container of '.filter__sub-item' elements
    var subFiltersItems = document.getElementsByClassName('filter__sub-item');
    var buttonCounter = document.getElementById('btn__counter');

    buttonFilters.addEventListener('click', function() {
      filtersLayout.classList.toggle('filter__layout--active');
      buttonCategories.setAttribute('data-state', 'disabled');
      if (document.documentElement.clientWidth < 700) {
        buttonCategories.classList.toggle('filter__button--hidden');
      }
      if (categoriesLayout.classList.contains('filter__layout--active')) {
        categoriesLayout.classList.remove('filter__layout--active')
      }
      if (this.getAttribute('data-state') === 'disabled') {
        this.setAttribute('data-state', 'enabled');
      } else {
        this.setAttribute('data-state', 'disabled');
      }
    });

    filters.addEventListener('click', function(event) {
      var target = event.target;
      while (target !== this) {
        if (target.hasAttribute('data-for')) {
          removeCssClass(filterItems, 'filter__item--active');
          target.classList.add('filter__item--active');

          var id = target.getAttribute('data-for');
          var subFilter = document.getElementById(id);

          removeCssClass(subFilters, 'filter__sub-filters--active');
          subFilter.classList.add('filter__sub-filters--active');

        }
        target = target.parentNode;
      }
    });

    for (var i = 0; i < subFiltersItems.length; i++) {
      subFiltersItems[i].addEventListener('click', function(event) {
        this.classList.toggle('filter__sub-item--active');
        buttonCounter.textContent = document.getElementsByClassName('filter__sub-item--active').length;

        var currentFilterCounter = this.parentNode.getElementsByClassName('filter__sub-item--active').length;
        var currentFilterDataFor = this.parentNode.getAttribute('id');
        // var currentFilter = document.getElementById(currentFilterId);
        var currentFilter = document.querySelector('[data-for="' + currentFilterDataFor +'"]');

        if (currentFilterCounter > 0) {
          currentFilter.classList.add('filter__item--active-sub-filters');
        } else if (currentFilterCounter === 0) {
          currentFilter.classList.remove('filter__item--active-sub-filters');
        }
      });
    }
  }


  window.addEventListener('resize', function () {
    if (document.documentElement.clientWidth > 700) {
      buttonCategories.classList.remove('filter__button--hidden');
      buttonFilters.classList.remove('filter__button--hidden');
    }
  });

  /**
   *
   * @param {array} list
   * @param {string} cssClass
   */
  function removeCssClass(list, cssClass) {
    for (var i = 0; i < list.length; i++) {
      list[i].classList.remove(cssClass);
    }
  }
})();
