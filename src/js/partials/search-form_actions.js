(function () {
  var searchForm = document.getElementById('form__search');
  if (searchForm) {
    var searchButton = document.getElementById('btn__search');
    var searchInput = document.getElementById('search-input');
    var searchButtonClose = document.getElementById('btn__search-close');
    searchButton.addEventListener('click', function() {
      if (document.documentElement.clientWidth > 1020) {
        searchForm.classList.add('search__form--active');
        setTimeout(function(){
          searchButton.setAttribute('type', 'submit');
        }, 1000);
      } else if (document.documentElement.clientWidth <= 1020) {
        searchInput.classList.add('search__input--active');
        searchButtonClose.classList.add('search__button-close--active');
        setTimeout(function(){
          searchButton.setAttribute('type', 'submit');
        }, 1000);
      }

    });

    searchButtonClose.addEventListener('click', function() {
      searchInput.classList.remove('search__input--active');
      this.classList.remove('search__button-close--active');
      searchButton.setAttribute('type', 'button');
    });


    document.addEventListener('click', function(event) {
      var target = event.target;
      while (target !== this) {
        if (target === searchForm) {
          return;
        }
        target = target.parentNode;
      }
      searchButton.setAttribute('type', 'button');
      searchForm.classList.remove('search__form--active');
    });
  }
})();