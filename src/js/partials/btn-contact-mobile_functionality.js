(function () {
  var btnContact = document.getElementById('btn__contact-us');
  if (btnContact) {
    var btnCall = document.getElementById('btn__call');
    var btnEmail = document.getElementById('btn__email');
    btnContact.addEventListener('click', function(event) {
      event.preventDefault();
      btnCall.classList.toggle('navigation__button--visible');
      btnEmail.classList.toggle('navigation__button--visible');
    });
  }
})();
