(function () {
  var link_navigation = document.getElementsByClassName('scroll-to');
  if (link_navigation) {
    var speed = 0.2;
    var link_navigation_length = link_navigation.length;
    for (var i = 0; i < link_navigation_length; i++) {
      link_navigation[i].addEventListener('click', function (event) {
        event.preventDefault();
        var scrolling = window.pageYOffset;
        var target = this.getAttribute('data-anchor');
        var indent = document.getElementById(target).getBoundingClientRect().top - 100;
        var start = null;
        requestAnimationFrame(step);
        function step(time) {
          if (start === null) start = time;
          var progress = time - start;
          var position = (indent < 0 ? Math.max(scrolling - progress / speed, scrolling + indent) : Math.min(scrolling + progress / speed, scrolling + indent))
          window.scrollTo(0, position);
          if (position !== scrolling + indent) {
            requestAnimationFrame(step)
          } else {
            location.target = target;
          }
        }
      }, false);
    }
  }
})();