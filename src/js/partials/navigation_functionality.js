(function () {
  var navItemsList = document.getElementsByClassName('navigation__item--has-subnavigation');
  if (navItemsList) {
    var subNavTitlesList = document.getElementsByClassName('sub-navigation__list-title');

    for (var i = 0; i < navItemsList.length; i++) {
      navItemsList[i].setAttribute('data-state', 'disabled');
      navItemsList[i].addEventListener('click', function (event) {
        if (document.documentElement.clientWidth < 1020) {
          event.preventDefault();
        }
        var target = event.target;
        var dropdownState = this.getAttribute('data-state');
        if (dropdownState === 'disabled') {
          for (var i = 0; i < navItemsList.length; i++) {
            navItemsList[i].setAttribute('data-state', 'disabled');
          }
          this.setAttribute('data-state', 'enabled');
        } else {
          if (target.classList.contains('navigation__link')) {
            this.setAttribute('data-state', 'disabled');
          }
        }
      });
    }

    for (var i = 0; i < subNavTitlesList.length; i++) {
      subNavTitlesList[i].setAttribute('data-state', 'disabled');
      subNavTitlesList[i].addEventListener('click', function (event) {
        if (document.documentElement.clientWidth < 1020) {
          event.preventDefault();
        }
        var target = event.target;
        var dropdownState = this.getAttribute('data-state');
        if (dropdownState === 'disabled') {
          for (var i = 0; i < subNavTitlesList.length; i++) {
            subNavTitlesList[i].setAttribute('data-state', 'disabled');
          }
          this.setAttribute('data-state', 'enabled');
        } else {
          this.setAttribute('data-state', 'disabled');
        }
      });
    }
  }
})();
