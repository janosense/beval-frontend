'use strict'

const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();
const del = require('del');
const gulp = require('gulp');
const csso = require('gulp-csso');
const htmlmin = require('gulp-htmlmin');
const include = require('gulp-include');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const uncss = require('gulp-uncss');
const rename = require('gulp-rename');

const path = {
  dist: {
    css: 'dist/css/',
    js: 'dist/js/',
    html: 'dist/',
    img: 'dist/img',
    fonts: 'dist/fonts'
  },
  src: {
    sass: 'src/sass/*.sass',
    js: 'src/js/*.js',
    html: 'src/*.html',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: {
    sass: 'src/sass/**/*.sass',
    js: 'src/js/**/*.js',
    html: 'src/**/*.html',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  remove: 'dist/',
  sourcemaps: '../sourcemaps'
};

gulp.task('js:build', function () {
  return gulp.src(path.src.js)
    .pipe(sourcemaps.init())
    .pipe(include()).on('error', console.log)
    // .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sourcemaps.write(path.sourcemaps))
    .pipe(gulp.dest(path.dist.js))
  // .pipe(browserSync.stream());
});

gulp.task('css:build', function () {
  return gulp.src(path.src.sass)
  // .pipe(sourcemaps.init())
    .pipe(sass())
    // .pipe(postcss([autoprefixer({
    //     browsers: ['last 2 versions']
    // })]))
    // .pipe(csso())
    .pipe(rename({
      suffix: '.min'
    }))
    // .pipe(sourcemaps.write(path.sourcemaps))
    .pipe(gulp.dest(path.dist.css))
  // .pipe(browserSync.stream());
});

gulp.task('html:build', function () {
  return gulp.src(path.src.html)
    .pipe(include()).on('error', console.log)
    // .pipe(htmlmin({
    //     removeComments: true,
    //     collapseWhitespace: true,
    //     collapseInlineTagWhitespace: true,
    //     conservativeCollapse: true
    // }))
    .pipe(gulp.dest(path.dist.html))
  // .pipe(browserSync.stream());
});

gulp.task('img:build', function () {
  console.log('Use https://kraken.io/ ;)');
  return gulp.src(path.src.img)
    .pipe(gulp.dest(path.dist.img));
});

gulp.task('fonts:build', function () {
  return gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.dist.fonts));
});

gulp.task('serve', function () {
  browserSync.watch('dist/**/*.*').on('change', browserSync.reload);
  browserSync.init({
    server: 'dist',
    notify: false
  });
});

gulp.task('remove:build', function () {
  del(path.remove);
});

gulp.task('watch', function () {
  gulp.watch(path.watch.sass, gulp.series('css:build'));
  gulp.watch(path.watch.js, gulp.series('js:build'));
  gulp.watch(path.watch.html, gulp.series('html:build'));
  gulp.watch(path.watch.img, gulp.series('img:build'));
  gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
});

gulp.task('build', gulp.series(
  'html:build',
  'css:build',
  'js:build',
  'img:build',
  'fonts:build'
));

gulp.task('default', gulp.series(
  'build',
  gulp.parallel(
    'serve',
    'watch')
));